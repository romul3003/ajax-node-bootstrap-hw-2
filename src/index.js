import './css/index.css'
import './less/index.less'
import './scss/index.scss'

const ipBtn = document.getElementById('ipBtn')

ipBtn.addEventListener('click', onBtnHandler)

async function onBtnHandler() {
	const fields = ['continent', 'country', 'region', 'city', 'district']

	getData('https://api.ipify.org/?format=json')
		.then(result => getData(`http://ip-api.com/json/${result.ip}?lang=ru&fields=${fields.join(',')}`))
		.then(result => render(result))
		.catch((e) => console.log(`${e.name}: ${e.message}`))
}

async function getData(url) {
	try {
		return await fetchData(url)
	} catch (e) {
		console.log(`${e.name}: ${e.message}`)
	}
}

async function fetchData(url) {
	const response = await fetch(url)

	if (response.ok) {

		return response.json()
	} else {
		console.log("Ошибка HTTP: " + response.status);
	}
}

function render(data) {
	const content = document.querySelector('.content')
	const ul = document.createElement('ul')
	ul.classList.add('list-group')

	for (let key in data) {
		if (key !== 'ip' && data.hasOwnProperty(key)) {
			ul.insertAdjacentHTML('beforeend', `<li class="list-group-item">${key}: ${data[key]}</li>`)
		}
	}

	const oldUl = content.querySelector('.list-group')

	if (oldUl) oldUl.remove()

	content.append(ul)
}